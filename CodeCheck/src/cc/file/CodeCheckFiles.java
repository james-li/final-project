package cc.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import cc.CodeCheckApp;
import cc.data.CodeCheckData;
import cc.data.Slide;

/**
 * This class serves as the file component for the Code Check app.
 * It provides all saving and loading services for the application.
 * 
 * @author Richard McKenna
 */
public class CodeCheckFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    CodeCheckApp app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
   
    //static final String JSON_SLIDE = "slide";
    static final String JSON_SLIDES1 = "slides1";
    static final String JSON_SLIDES2 = "slides2";
    static final String JSON_SLIDES3 = "slides3";
    static final String JSON_SLIDES4 = "slides4";
    static final String JSON_SLIDES5 = "slides5";
    static final String JSON_FILE_NAME1 = "file_name1";
    static final String JSON_FILE_NAME2 = "file_name2";
    static final String JSON_FILE_NAME3 = "file_name3";
    static final String JSON_FILE_NAME4 = "file_name4";
    static final String JSON_FILE_NAME5 = "file_name5";
    static final String JSON_FILE_PATH1 = "file_path1";
    static final String JSON_FILE_PATH2 = "file_path2";
    static final String JSON_FILE_PATH3 = "file_path3";
    static final String JSON_FILE_PATH4 = "file_path4";
    static final String JSON_FILE_PATH5 = "file_path5";
    static final String JSON_TITLE = "title";
    
    public CodeCheckFiles(CodeCheckApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);

	// CLEAR THE OLD DATA OUT
	CodeCheckData dataManager = (CodeCheckData)data;
        dataManager.resetData();

        // NOW LOAD ALL THE DATA FROM THE json OBJECT
        JsonArray jsonSlidesArray1 = json.getJsonArray(JSON_SLIDES1);
        for (int i = 0; i < jsonSlidesArray1.size(); i++) {
            JsonObject jsonSlide = jsonSlidesArray1.getJsonObject(i);
            String fileName = jsonSlide.getString(JSON_FILE_NAME1);
            String pathName = jsonSlide.getString(JSON_FILE_PATH1);
            dataManager.addSlide1(fileName, pathName);
        }
        JsonArray jsonSlidesArray2 = json.getJsonArray(JSON_SLIDES2);
        for (int i = 0; i < jsonSlidesArray2.size(); i++) {
            JsonObject jsonSlide = jsonSlidesArray2.getJsonObject(i);
            String fileName = jsonSlide.getString(JSON_FILE_NAME2);
            String pathName = jsonSlide.getString(JSON_FILE_PATH2);
            dataManager.addSlide2(fileName, pathName);
        }
        JsonArray jsonSlidesArray3 = json.getJsonArray(JSON_SLIDES3);
        for (int i = 0; i < jsonSlidesArray3.size(); i++) {
            JsonObject jsonSlide = jsonSlidesArray3.getJsonObject(i);
            String fileName = jsonSlide.getString(JSON_FILE_NAME3);
            String pathName = jsonSlide.getString(JSON_FILE_PATH3);
            dataManager.addSlide3(fileName, pathName);
        }  
        JsonArray jsonSlidesArray4 = json.getJsonArray(JSON_SLIDES4);
        for (int i = 0; i < jsonSlidesArray4.size(); i++) {
            JsonObject jsonSlide = jsonSlidesArray4.getJsonObject(i);
            String fileName = jsonSlide.getString(JSON_FILE_NAME4);
            String pathName = jsonSlide.getString(JSON_FILE_PATH4);
            dataManager.addSlide4(fileName, pathName);
        }  
        JsonArray jsonSlidesArray5 = json.getJsonArray(JSON_SLIDES5);
        for (int i = 0; i < jsonSlidesArray5.size(); i++) {
            JsonObject jsonSlide = jsonSlidesArray5.getJsonObject(i);
            String fileName = jsonSlide.getString(JSON_FILE_NAME5);
            String pathName = jsonSlide.getString(JSON_FILE_PATH5);
            dataManager.addSlide5(fileName, pathName);
        }  
        JsonArray jsonTitleArray = json.getJsonArray(JSON_TITLE);
        for (int i = 0; i < jsonTitleArray.size(); i++) {
            JsonObject jsonTitle = jsonTitleArray.getJsonObject(i);
            String title = jsonTitle.getString(JSON_TITLE);
            app.getGUI().getWindow().setTitle(title);
            
        }  
        
    }
      
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        
	// GET THE DATA
	CodeCheckData dataManager = (CodeCheckData)data;
        
	// NOW BUILD THE SLIDES JSON OBJECTS TO SAVE
	JsonArrayBuilder slidesArrayBuilder1 = Json.createArrayBuilder();
	ObservableList<Slide> slides1 = dataManager.getSlides1();
	for (Slide slide : slides1) {	    
	    JsonObject slideJson = Json.createObjectBuilder()
		    .add(JSON_FILE_NAME1, slide.getFileName())
                    .add(JSON_FILE_PATH1, slide.getPath()).build();
	    slidesArrayBuilder1.add(slideJson);
	}
        JsonArrayBuilder slidesArrayBuilder2 = Json.createArrayBuilder();
	ObservableList<Slide> slides2 = dataManager.getSlides2();
	for (Slide slide : slides2) {	    
	    JsonObject slideJson = Json.createObjectBuilder()
		    .add(JSON_FILE_NAME2, slide.getFileName())
                    .add(JSON_FILE_PATH2, slide.getPath()).build();
	    slidesArrayBuilder2.add(slideJson);
	}
        JsonArrayBuilder slidesArrayBuilder3 = Json.createArrayBuilder();
	ObservableList<Slide> slides3 = dataManager.getSlides3();
	for (Slide slide : slides3) {	    
	    JsonObject slideJson = Json.createObjectBuilder()
		    .add(JSON_FILE_NAME3, slide.getFileName())
                    .add(JSON_FILE_PATH3, slide.getPath()).build();
	    slidesArrayBuilder3.add(slideJson);
	}
        JsonArrayBuilder slidesArrayBuilder4 = Json.createArrayBuilder();
	ObservableList<Slide> slides4 = dataManager.getSlides4();
	for (Slide slide : slides4) {	    
	    JsonObject slideJson = Json.createObjectBuilder()
		    .add(JSON_FILE_NAME4, slide.getFileName())
                    .add(JSON_FILE_PATH4, slide.getPath()).build();
	    slidesArrayBuilder4.add(slideJson);
	}
        JsonArrayBuilder slidesArrayBuilder5 = Json.createArrayBuilder();
	ObservableList<Slide> slides5 = dataManager.getSlides5();
	for (Slide slide : slides5) {	    
	    JsonObject slideJson = Json.createObjectBuilder()
		    .add(JSON_FILE_NAME5, slide.getFileName())
                    .add(JSON_FILE_PATH5, slide.getPath()).build();
	    slidesArrayBuilder5.add(slideJson);
	}
        JsonArrayBuilder titleArrayBuilder = Json.createArrayBuilder();
        JsonObject title = Json.createObjectBuilder().add(JSON_TITLE, app.getGUI().getWindow().getTitle()).build();
        titleArrayBuilder.add(title);
        
	JsonArray slides1Array = slidesArrayBuilder1.build();
        JsonArray slides2Array = slidesArrayBuilder2.build();
        JsonArray slides3Array = slidesArrayBuilder3.build();
        JsonArray slides4Array = slidesArrayBuilder4.build();
        JsonArray slides5Array = slidesArrayBuilder5.build();
        JsonArray titleArray = titleArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_SLIDES1, slides1Array)
                .add(JSON_SLIDES2, slides2Array)
                .add(JSON_SLIDES3, slides3Array)
                .add(JSON_SLIDES4, slides4Array)
                .add(JSON_SLIDES5, slides5Array)
                .add(JSON_TITLE, titleArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}