package cc;

/**
 * This enum provides a list of all the user interface
 * text that needs to be loaded from the XML properties
 * file. By simply changing the XML file we could initialize
 * this application such that all UI controls are provided
 * in another language.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public enum CodeCheckProp {
    // FOR SIMPLE OK/CANCEL DIALOG BOXES
    OK_PROMPT,
    CANCEL_PROMPT,
    
    // THESE ARE FOR TEXT PARTICULAR TO THE APP'S WORKSPACE CONTROLS
    SLIDES_HEADER_TEXT,
    HOME_BUTTON_TEXT,
    PREVIOUS_BUTTON_TEXT,
    NEXT_BUTTON_TEXT,
    EXTRACT_COLUMN_TEXT,
    RENAME_COLUMN_TEXT,
    UNZIP_COLUMN_TEXT,
    EXTRACT_CODE_COLUMN_TEXT,
    CHECK_COLUMN_TEXT,
    REMOVE_BUTTON_TEXT,
    REFRESH_BUTTON_TEXT,
    VIEW_BUTTON_TEXT,
    
    // PANE BUTTONS
    STEP_ONE_LABEL,
    STEP_ONE_CAPTION,
    STEP_ONE_PROGRESS,
    EXTRACT_ICON,
    
    STEP_TWO_LABEL,
    STEP_TWO_CAPTION,
    STEP_TWO_PROGRESS,
    RENAME_ICON,
    
    STEP_THREE_LABEL,
    STEP_THREE_CAPTION,
    STEP_THREE_PROGRESS,
    UNZIP_ICON,
    
    STEP_FOUR_LABEL,
    STEP_FOUR_CAPTION,
    STEP_FOUR_PROGRESS,
    EXTRACT_CODE_ICON,
    SOURCE_TYPE_LABEL,
    JAVA_LABEL,
    JS_LABEL,
    C_H_CPP_LABEL,
    CS_LABEL,
    OTHER_LABEL,
    
    STEP_FIVE_LABEL,
    STEP_FIVE_CAPTION,
    STEP_FIVE_PROGRESS,
    CHECK_ICON,
    VIEW_RESULTS_ICON,
    
    // FOR OTHER INTERACTIONS
    APP_PATH_WORK,
    INVALID_IMAGE_PATH_TITLE,
    INVALID_IMAGE_PATH_MESSAGE

}
