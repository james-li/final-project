package cc.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import cc.CodeCheckApp;

/**
 * This is the data component for CodeCheckApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 */
public class CodeCheckData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    CodeCheckApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<Slide> slides1;
    ObservableList<Slide> slides2;
    ObservableList<Slide> slides3;
    ObservableList<Slide> slides4;
    ObservableList<Slide> slides5;
    

    /**
     * This constructor will setup the required data structures for use.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public CodeCheckData(CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        
        // MAKE THE SLIDES MODEL
        slides1 = FXCollections.observableArrayList();
        slides2 = FXCollections.observableArrayList();
        slides3 = FXCollections.observableArrayList();
        slides4 = FXCollections.observableArrayList();
        slides5 = FXCollections.observableArrayList();
    }
    
    // ACCESSOR METHOD
    public ObservableList<Slide> getSlides1() {
        return slides1;
    }
    public ObservableList<Slide> getSlides2() {
        return slides2;
    }
    public ObservableList<Slide> getSlides3() {
        return slides3;
    }
    public ObservableList<Slide> getSlides4() {
        return slides4;
    }
    public ObservableList<Slide> getSlides5() {
        return slides5;
    }
    
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {

    }

    // FOR ADDING A SLIDE WHEN THERE ISN'T A CUSTOM SIZE
    public void addSlide1(String fileName, String pathName) {
        Slide slideToAdd = new Slide(fileName, pathName);
        slides1.add(slideToAdd);
    }
    public void addSlide2(String fileName, String pathName) {
        Slide slideToAdd = new Slide(fileName, pathName);
        slides2.add(slideToAdd);
    }
    public void addSlide3(String fileName, String pathName) {
        Slide slideToAdd = new Slide(fileName, pathName);
        slides3.add(slideToAdd);
    }
    public void addSlide4(String fileName, String pathName) {
        Slide slideToAdd = new Slide(fileName, pathName);
        slides4.add(slideToAdd);
    }
    public void addSlide5(String fileName, String pathName) {
        Slide slideToAdd = new Slide(fileName, pathName);
        slides5.add(slideToAdd);
    }
  
}