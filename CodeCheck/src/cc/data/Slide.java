package cc.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This data class represents a single slide in a slide show. Note that
 * it has information regarding the path to the file, the caption for
 * the slide, and sizing information.
 */
public class Slide {
    private StringProperty fileNameProperty;
    private StringProperty pathProperty;
    
    public Slide(String initFileName, String initPathName) {
        fileNameProperty = new SimpleStringProperty(initFileName);
        pathProperty = new SimpleStringProperty(initPathName);
    }
    
    // ACCESSORS AND MUTATORS
    
    public StringProperty getFileNameProperty() {
        return fileNameProperty;
    }
    public String getFileName() {
        return fileNameProperty.getValue();
    }
    public void setFileName(String initFileName) {
        fileNameProperty.setValue(initFileName);
    }
    
    public StringProperty getPathProperty() {
        return pathProperty;
    }
    public String getPath() {
        return pathProperty.getValue();
    }
    public void setPath(String initPath) {
        pathProperty.setValue(initPath);
    }
}
