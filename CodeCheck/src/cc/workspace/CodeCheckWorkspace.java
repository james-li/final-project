package cc.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import cc.CodeCheckApp;
import static cc.CodeCheckProp.CHECK_COLUMN_TEXT;
import static cc.CodeCheckProp.CHECK_ICON;
import static cc.CodeCheckProp.CS_LABEL;
import static cc.CodeCheckProp.C_H_CPP_LABEL;
import static cc.CodeCheckProp.EXTRACT_CODE_COLUMN_TEXT;
import static cc.CodeCheckProp.EXTRACT_CODE_ICON;
import static cc.CodeCheckProp.EXTRACT_COLUMN_TEXT;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import properties_manager.PropertiesManager;
import static cc.CodeCheckProp.EXTRACT_ICON;
import static cc.CodeCheckProp.HOME_BUTTON_TEXT;
import static cc.CodeCheckProp.JAVA_LABEL;
import static cc.CodeCheckProp.JS_LABEL;
import static cc.CodeCheckProp.NEXT_BUTTON_TEXT;
import static cc.CodeCheckProp.OTHER_LABEL;
import static cc.CodeCheckProp.PREVIOUS_BUTTON_TEXT;
import static cc.CodeCheckProp.REFRESH_BUTTON_TEXT;
import static cc.CodeCheckProp.REMOVE_BUTTON_TEXT;
import static cc.CodeCheckProp.RENAME_COLUMN_TEXT;
import static cc.CodeCheckProp.RENAME_ICON;
import static cc.CodeCheckProp.SOURCE_TYPE_LABEL;
import static cc.CodeCheckProp.STEP_FIVE_CAPTION;
import static cc.CodeCheckProp.STEP_FIVE_LABEL;
import static cc.CodeCheckProp.STEP_FIVE_PROGRESS;
import static cc.CodeCheckProp.STEP_FOUR_CAPTION;
import static cc.CodeCheckProp.STEP_FOUR_LABEL;
import static cc.CodeCheckProp.STEP_FOUR_PROGRESS;
import static cc.CodeCheckProp.STEP_ONE_CAPTION;
import static cc.CodeCheckProp.STEP_ONE_LABEL;
import static cc.CodeCheckProp.STEP_ONE_PROGRESS;
import static cc.CodeCheckProp.STEP_THREE_CAPTION;
import static cc.CodeCheckProp.STEP_THREE_LABEL;
import static cc.CodeCheckProp.STEP_THREE_PROGRESS;
import static cc.CodeCheckProp.STEP_TWO_CAPTION;
import static cc.CodeCheckProp.STEP_TWO_LABEL;
import static cc.CodeCheckProp.STEP_TWO_PROGRESS;
import static cc.CodeCheckProp.UNZIP_COLUMN_TEXT;
import static cc.CodeCheckProp.UNZIP_ICON;
import static cc.CodeCheckProp.VIEW_BUTTON_TEXT;
import static cc.CodeCheckProp.VIEW_RESULTS_ICON;
import cc.data.Slide;
import cc.data.CodeCheckData;
import static cc.style.CodeCheckStyle.CLASS_EDIT_BUTTON;
import static cc.style.CodeCheckStyle.CLASS_SLIDES_TABLE;
import java.util.ArrayList;
import javafx.beans.binding.Bindings;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * This class serves as the workspace component for the Code Check
 * application. It provides all the user interface controls in 
 * the workspace area.
 * 
 * @author James Li
 */
public class CodeCheckWorkspace extends AppWorkspaceComponent {
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    CodeCheckApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    CodeCheckController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editToolbar;
    Button homeButton;
    Button previousButton;
    Button nextButton;
    
    // EXTRACT PANE
    ScrollPane extractScrollPane;
    TableView<Slide> extractTableView;
    TableColumn<Slide, StringProperty> blackboardSubmissions;
    
    VBox textBoxes1 = new VBox();
    TextArea successBox1 = new TextArea();
    TextArea errorBox1 = new TextArea();
    ScrollPane successScrollPane1;
    ScrollPane errorScrollPane1;
    
    Button extractButton;
    HBox progressBox1 = new HBox();
    ProgressBar stepOneProgressBar =  new ProgressBar(0);
    ProgressIndicator stepOneProgressIndicator = new ProgressIndicator(0);
    
    HBox bottomToolbar1 = new HBox();
    Button removeButton1;
    Button refreshButton1;
    Button viewButton1;
    
    // RENAME PANE
    ScrollPane renameScrollPane;
    TableView<Slide> renameTableView;
    TableColumn<Slide, StringProperty> studentSubmissions;
    
    VBox textBoxes2 = new VBox();
    TextArea successBox2 = new TextArea();
    TextArea errorBox2 = new TextArea();
    ScrollPane successScrollPane2;
    ScrollPane errorScrollPane2;
    
    Button renameButton;
    HBox progressBox2 = new HBox();
    ProgressBar stepTwoProgressBar;
    ProgressIndicator stepTwoProgressIndicator;
    
    HBox bottomToolbar2 = new HBox();
    Button removeButton2;
    Button refreshButton2;
    Button viewButton2;
    
    // UNZIP PANE
    ScrollPane unzipScrollPane;
    TableView<Slide> unzipTableView;
    TableColumn<Slide, StringProperty> studentZIPFiles;
    
    VBox textBoxes3 = new VBox();
    TextArea successBox3 = new TextArea();
    TextArea errorBox3 = new TextArea();
    ScrollPane successScrollPane3;
    ScrollPane errorScrollPane3;
    
    Button unzipButton;
    HBox progressBox3 = new HBox();
    ProgressBar stepThreeProgressBar;
    ProgressIndicator stepThreeProgressIndicator;
    
    HBox bottomToolbar3 = new HBox();
    Button removeButton3;
    Button refreshButton3;
    Button viewButton3;
    
    // EXTRACT CODE PANE
    ScrollPane extractCodeScrollPane;
    TableView<Slide> extractCodeTableView;
    TableColumn<Slide, StringProperty> studentWorkDirectories;
    
    VBox textBoxes4 = new VBox();
    TextArea successBox4 = new TextArea();
    TextArea errorBox4 = new TextArea();
    ScrollPane successScrollPane4;
    ScrollPane errorScrollPane4;
    
    Button extractCodeButton;
    HBox progressBox4 = new HBox();
    ProgressBar stepFourProgressBar;
    ProgressIndicator stepFourProgressIndicator;
    CheckBox javaCheckBox;
    CheckBox jsCheckBox;
    CheckBox chcppCheckBox;
    CheckBox csCheckBox;
    CheckBox otherCheckBox;
    TextField otherLabel;
    GridPane sourceFilesGridPane;
    
    HBox bottomToolbar4 = new HBox();
    Button removeButton4;
    Button refreshButton4;
    Button viewButton4;
    
    // CODE CHECK PANE
    ScrollPane checkScrollPane;
    TableView<Slide> checkTableView;
    TableColumn<Slide, StringProperty> studentWork;
    
    VBox textBoxes5 = new VBox();
    TextArea successBox5 = new TextArea();
    ScrollPane successScrollPane5;
    
    HBox checkButtonPane;
    Button checkButton;
    Button viewResultsButton;
    HBox progressBox5 = new HBox();
    ProgressBar stepFiveProgressBar;
    ProgressIndicator stepFiveProgressIndicator;
    
    HBox bottomToolbar5 = new HBox();
    Button removeButton5;
    Button refreshButton5;
    Button viewButton5;
    
    // DIFFERENT PANES
    GridPane extractPane = new GridPane();
    GridPane renamePane = new GridPane();
    GridPane unzipPane = new GridPane();
    GridPane extractCodePane = new GridPane();
    GridPane checkPane = new GridPane();
    ArrayList<Pane> panes = new ArrayList<Pane>();
    int currentPaneIndex = 0;
    Pane workspacePane = new Pane();
    
    /**
     * The constructor initializes the user interface for the
     * workspace area of the application.
     */
    public CodeCheckWorkspace(CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();        
        
    }
    
    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager(); 
       
        panes.add(extractPane);
        panes.add(renamePane);
        panes.add(unzipPane);
        panes.add(extractCodePane);
        panes.add(checkPane);
        
        // FIRST MAKE ALL THE COMPONENTS
        editToolbar = new HBox();
        homeButton = new Button(props.getProperty(HOME_BUTTON_TEXT));
        previousButton = new Button(props.getProperty(PREVIOUS_BUTTON_TEXT));
        nextButton = new Button(props.getProperty(NEXT_BUTTON_TEXT));
        editToolbar.getChildren().add(homeButton);
        editToolbar.getChildren().add(previousButton);
        editToolbar.getChildren().add(nextButton);
        
        // EXTRACT PANE
        extractScrollPane = new ScrollPane();
        successScrollPane1 = new ScrollPane();
        errorScrollPane1 = new ScrollPane();
        extractTableView = new TableView();
        blackboardSubmissions = new TableColumn(props.getProperty(EXTRACT_COLUMN_TEXT));
        removeButton1 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        removeButton1.disableProperty().bind(extractTableView.getSelectionModel().selectedItemProperty().isNull());
        refreshButton1 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton1 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        viewButton1.disableProperty().bind(extractTableView.getSelectionModel().selectedItemProperty().isNull());
        
        extractTableView.getColumns().add(blackboardSubmissions);
        blackboardSubmissions.prefWidthProperty().bind(extractTableView.widthProperty().divide(1));
        blackboardSubmissions.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );
        
        CodeCheckData data = (CodeCheckData)app.getDataComponent();
        ObservableList<Slide> model = data.getSlides1();
        extractTableView.setItems(model);
        extractTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //extractButton.disableProperty().bind(extractTableView.getSelectionModel().selectedItemProperty().isNull());
        
        extractScrollPane.setContent(extractTableView);
        bottomToolbar1.getChildren().addAll(removeButton1, refreshButton1, viewButton1);
        bottomToolbar1.setSpacing(30);
        
        textBoxes1.getChildren().add(successBox1);
        textBoxes1.getChildren().add(errorBox1);
        successScrollPane1.setContent(successBox1);
        errorScrollPane1.setContent(errorBox1);
        successBox1.setEditable(false);
        errorBox1.setEditable(false);
        
        // RENAME PANE
        renameScrollPane = new ScrollPane();
        successScrollPane2 = new ScrollPane();
        errorScrollPane2 = new ScrollPane();
        renameTableView = new TableView();
        studentSubmissions = new TableColumn(props.getProperty(RENAME_COLUMN_TEXT));
        removeButton2 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        removeButton2.disableProperty().bind(renameTableView.getSelectionModel().selectedItemProperty().isNull());
        refreshButton2 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton2 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        viewButton2.disableProperty().bind(renameTableView.getSelectionModel().selectedItemProperty().isNull());
        
        renameTableView.getColumns().add(studentSubmissions);
        studentSubmissions.prefWidthProperty().bind(renameTableView.widthProperty().divide(1));
        studentSubmissions.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );
        
        CodeCheckData data2 = (CodeCheckData)app.getDataComponent();
        ObservableList<Slide> model2 = data2.getSlides2();
        renameTableView.setItems(model2);
        //renameTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
         
        renameScrollPane.setContent(renameTableView);
        bottomToolbar2.getChildren().addAll(removeButton2, refreshButton2, viewButton2);
        bottomToolbar2.setSpacing(30);
        
        textBoxes2.getChildren().add(successBox2);
        textBoxes2.getChildren().add(errorBox2);
        successScrollPane2.setContent(successBox2);
        errorScrollPane2.setContent(errorBox2);
        successBox2.setEditable(false);
        errorBox2.setEditable(false);
        
        // UNZIP PANE
        unzipScrollPane = new ScrollPane();
        successScrollPane3 = new ScrollPane();
        errorScrollPane3 = new ScrollPane();
        unzipTableView = new TableView();
        studentZIPFiles = new TableColumn(props.getProperty(UNZIP_COLUMN_TEXT));
        removeButton3 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        removeButton3.disableProperty().bind(unzipTableView.getSelectionModel().selectedItemProperty().isNull());
        refreshButton3 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton3 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        viewButton3.disableProperty().bind(unzipTableView.getSelectionModel().selectedItemProperty().isNull());
        
        unzipTableView.getColumns().add(studentZIPFiles);
        studentZIPFiles.prefWidthProperty().bind(unzipTableView.widthProperty().divide(1));
        studentZIPFiles.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );
        
        CodeCheckData data3 = (CodeCheckData)app.getDataComponent();
        ObservableList<Slide> model3 = data3.getSlides3();
        unzipTableView.setItems(model3);
         
        unzipScrollPane.setContent(unzipTableView);
        bottomToolbar3.getChildren().addAll(removeButton3, refreshButton3, viewButton3);
        bottomToolbar3.setSpacing(30);
        unzipTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        textBoxes3.getChildren().add(successBox3);
        textBoxes3.getChildren().add(errorBox3);
        successScrollPane3.setContent(successBox3);
        errorScrollPane3.setContent(errorBox3);
        successBox3.setEditable(false);
        errorBox3.setEditable(false);
        
        // EXTRACT CODE PANE
        extractCodeScrollPane = new ScrollPane();
        successScrollPane4 = new ScrollPane();
        errorScrollPane4 = new ScrollPane();
        extractCodeTableView = new TableView();
        studentWorkDirectories = new TableColumn(props.getProperty(EXTRACT_CODE_COLUMN_TEXT));
        removeButton4 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        removeButton4.disableProperty().bind(extractCodeTableView.getSelectionModel().selectedItemProperty().isNull());
        refreshButton4 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton4 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        viewButton4.disableProperty().bind(extractCodeTableView.getSelectionModel().selectedItemProperty().isNull());
        
        extractCodeTableView.getColumns().add(studentWorkDirectories);
        studentWorkDirectories.prefWidthProperty().bind(extractCodeTableView.widthProperty().divide(1));
        studentWorkDirectories.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );
        
        CodeCheckData data4 = (CodeCheckData)app.getDataComponent();
        ObservableList<Slide> model4 = data4.getSlides4();
        extractCodeTableView.setItems(model4);
        extractCodeTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
         
        extractCodeScrollPane.setContent(extractCodeTableView);
        bottomToolbar4.getChildren().addAll(removeButton4, refreshButton4, viewButton4);
        bottomToolbar4.setSpacing(30);
        
        textBoxes4.getChildren().add(successBox4);
        textBoxes4.getChildren().add(errorBox4);
        successScrollPane4.setContent(successBox4);
        errorScrollPane4.setContent(errorBox4);
        successBox4.setEditable(false);
        errorBox4.setEditable(false);
        
        // CODE CHECK PANE
        checkScrollPane = new ScrollPane();
        successScrollPane5 = new ScrollPane();
        checkTableView = new TableView();
        studentWork = new TableColumn(props.getProperty(CHECK_COLUMN_TEXT));
        removeButton5 = new Button(props.getProperty(REMOVE_BUTTON_TEXT));
        removeButton5.disableProperty().bind(checkTableView.getSelectionModel().selectedItemProperty().isNull());
        refreshButton5 = new Button(props.getProperty(REFRESH_BUTTON_TEXT));
        viewButton5 = new Button(props.getProperty(VIEW_BUTTON_TEXT));
        viewButton5.disableProperty().bind(checkTableView.getSelectionModel().selectedItemProperty().isNull());
        
        checkTableView.getColumns().add(studentWork);
        studentWork.prefWidthProperty().bind(checkTableView.widthProperty().divide(1));
        studentWork.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );
        
        CodeCheckData data5 = (CodeCheckData)app.getDataComponent();
        ObservableList<Slide> model5 = data5.getSlides5();
        checkTableView.setItems(model5);
        checkTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
         
        checkScrollPane.setContent(checkTableView);
        bottomToolbar5.getChildren().addAll(removeButton5, refreshButton5, viewButton5);
        bottomToolbar5.setSpacing(30);
        
        textBoxes5.getChildren().add(successBox5);
        successScrollPane5.setContent(successBox5);
        successBox5.setEditable(false);
        
        // HOME, PREV, NEXT DISABLE
        homeButton.setDisable(true);
        previousButton.setDisable(true);
        nextButton.setDisable(true);
        
        // TOOLBAR
        app.getGUI().getTopToolbarPane().getChildren().add(editToolbar);
            
        // EXTRACT WORKSPACE 
        Label stepOne = new Label(props.getProperty(STEP_ONE_LABEL));
        Label stepOneCaption = new Label(props.getProperty(STEP_ONE_CAPTION));
        Label stepOneProgress = new Label(props.getProperty(STEP_ONE_PROGRESS));
        stepOneProgressBar = new ProgressBar(0);
        stepOneProgressIndicator = new ProgressIndicator(0);
        progressBox1 = new HBox();
        progressBox1.getChildren().addAll(stepOneProgress, stepOneProgressBar, stepOneProgressIndicator);
        progressBox1.setSpacing(30);
        extractButton = new Button(props.getProperty(EXTRACT_ICON));
        
        extractPane.add(stepOne, 0, 0);
        extractPane.add(stepOneCaption, 0, 1);
        extractPane.add(progressBox1, 1, 3);
        extractPane.add(extractButton, 1, 1);
        extractPane.setVgap(10);
        extractPane.setHgap(100);
        extractScrollPane.setFitToWidth(true);
        extractScrollPane.setFitToHeight(true);
        extractScrollPane.setPrefViewportHeight(100);
        extractScrollPane.setPrefViewportWidth(600);
        extractPane.add(extractScrollPane, 0, 2);
        extractPane.add(bottomToolbar1, 0, 3);
        extractPane.add(textBoxes1, 1, 2);  
        
        // RENAME WORKSPACE
        Label stepTwo = new Label(props.getProperty(STEP_TWO_LABEL));
        Label stepTwoCaption = new Label(props.getProperty(STEP_TWO_CAPTION));
        Label stepTwoProgress = new Label(props.getProperty(STEP_TWO_PROGRESS));
        stepTwoProgressBar = new ProgressBar(0);
        stepTwoProgressIndicator = new ProgressIndicator(0);
        progressBox2 = new HBox();
        progressBox2.getChildren().addAll(stepTwoProgress, stepTwoProgressBar, stepTwoProgressIndicator);
        progressBox2.setSpacing(30);
        renameButton = new Button(props.getProperty(RENAME_ICON));
        
        renamePane.add(stepTwo, 0, 0);
        renamePane.add(stepTwoCaption, 0, 1);
        renamePane.add(progressBox2, 1, 3);
        renamePane.add(renameButton, 1, 1);
        renamePane.setVgap(10);
        renamePane.setHgap(100);
        renameScrollPane.setFitToWidth(true);
        renameScrollPane.setFitToHeight(true);
        renameScrollPane.setPrefViewportHeight(100);
        renameScrollPane.setPrefViewportWidth(600);
        renamePane.add(renameScrollPane, 0, 2);
        renamePane.add(bottomToolbar2, 0, 3);
        renamePane.add(textBoxes2, 1, 2);
        
        // UNZIP WORKSPACE
        Label stepThree = new Label(props.getProperty(STEP_THREE_LABEL));
        Label stepThreeCaption = new Label(props.getProperty(STEP_THREE_CAPTION));
        Label stepThreeProgress = new Label(props.getProperty(STEP_THREE_PROGRESS));
        stepThreeProgressBar = new ProgressBar(0);
        stepThreeProgressIndicator = new ProgressIndicator(0);
        progressBox3 = new HBox();
        progressBox3.getChildren().addAll(stepThreeProgress, stepThreeProgressBar, stepThreeProgressIndicator);
        progressBox3.setSpacing(30);
        unzipButton = new Button(props.getProperty(UNZIP_ICON));
        
        unzipPane.add(stepThree, 0, 0);
        unzipPane.add(stepThreeCaption, 0, 1);
        unzipPane.add(progressBox3, 1, 3);
        unzipPane.add(unzipButton, 1, 1);
        unzipPane.setVgap(10);
        unzipPane.setHgap(100);
        unzipScrollPane.setFitToWidth(true);
        unzipScrollPane.setFitToHeight(true);
        unzipScrollPane.setPrefViewportHeight(100);
        unzipScrollPane.setPrefViewportWidth(600);
        unzipPane.add(unzipScrollPane, 0, 2);
        unzipPane.add(bottomToolbar3, 0, 3);
        unzipPane.add(textBoxes3, 1, 2);
        
        // EXTRACT CODE WORKSPACE
        Label stepFour = new Label(props.getProperty(STEP_FOUR_LABEL));
        Label stepFourCaption = new Label(props.getProperty(STEP_FOUR_CAPTION));
        Label stepFourProgress = new Label(props.getProperty(STEP_FOUR_PROGRESS));
        stepFourProgressBar = new ProgressBar(0);
        stepFourProgressIndicator = new ProgressIndicator(0);
        progressBox4 = new HBox();
        progressBox4.getChildren().addAll(stepFourProgress, stepFourProgressBar, stepFourProgressIndicator);
        progressBox4.setSpacing(30);
        extractCodeButton = new Button(props.getProperty(EXTRACT_CODE_ICON));
        
        TextField otherLabel = new TextField();
        javaCheckBox = new CheckBox(props.getProperty(JAVA_LABEL));
        jsCheckBox = new CheckBox(props.getProperty(JS_LABEL));
        chcppCheckBox = new CheckBox(props.getProperty(C_H_CPP_LABEL));
        csCheckBox = new CheckBox(props.getProperty(CS_LABEL));
        otherCheckBox = new CheckBox(props.getProperty(OTHER_LABEL));
        Label sourceLabel = new Label(props.getProperty(SOURCE_TYPE_LABEL));
        sourceFilesGridPane = new GridPane();
        sourceFilesGridPane.add(javaCheckBox, 0, 0);
        sourceFilesGridPane.add(jsCheckBox, 1, 0);
        sourceFilesGridPane.add(chcppCheckBox, 0, 1);
        sourceFilesGridPane.add(csCheckBox, 1, 1);
        sourceFilesGridPane.add(otherCheckBox, 0, 2);
        sourceFilesGridPane.add(otherLabel, 0, 3);
        
        extractCodePane.add(stepFour, 0, 0);
        extractCodePane.add(stepFourCaption, 0, 1);
        extractCodePane.add(progressBox4, 1, 3);
        extractCodePane.add(extractCodeButton, 1, 1);
        extractCodePane.setVgap(10);
        extractCodePane.setHgap(100);
        extractCodeScrollPane.setFitToWidth(true);
        extractCodeScrollPane.setFitToHeight(true);
        extractCodeScrollPane.setPrefViewportHeight(100);
        extractCodeScrollPane.setPrefViewportWidth(600);
        extractCodePane.add(extractCodeScrollPane, 0, 2);
        extractCodePane.add(bottomToolbar4, 0, 3);
        extractCodePane.add(textBoxes4, 1, 2);
        extractCodePane.add(sourceLabel, 0, 4);
        extractCodePane.add(sourceFilesGridPane, 0, 5);
        
        // CODE CHECK WORKSPACE
        Label stepFive = new Label(props.getProperty(STEP_FIVE_LABEL));
        Label stepFiveCaption = new Label(props.getProperty(STEP_FIVE_CAPTION));
        Label stepFiveProgress = new Label(props.getProperty(STEP_FIVE_PROGRESS));
        stepFiveProgressBar = new ProgressBar(0);
        stepFiveProgressIndicator = new ProgressIndicator(0);
        progressBox5 = new HBox();
        progressBox5.getChildren().addAll(stepFiveProgress, stepFiveProgressBar, stepFiveProgressIndicator);
        progressBox5.setSpacing(30);
        checkButtonPane = new HBox();
        checkButtonPane.setSpacing(30);
        checkButton = new Button(props.getProperty(CHECK_ICON));
        viewResultsButton = new Button(props.getProperty(VIEW_RESULTS_ICON));
        checkButtonPane.getChildren().addAll(checkButton, viewResultsButton);
        
        checkPane.add(stepFive, 0, 0);
        checkPane.add(stepFiveCaption, 0, 1);
        checkPane.add(progressBox5, 1, 3);
        checkPane.add(checkButtonPane, 1, 1);
        checkPane.setVgap(10);
        checkPane.setHgap(100);
        checkScrollPane.setFitToWidth(true);
        checkScrollPane.setFitToHeight(true);
        checkScrollPane.setPrefViewportHeight(200);
        checkScrollPane.setPrefViewportWidth(600);
        checkPane.add(checkScrollPane, 0, 2);
        checkPane.add(bottomToolbar5, 0, 3);
        checkPane.add(textBoxes5, 1, 2);
        
        // AND SET THIS AS THE WORKSPACE PANE 
        workspacePane.getChildren().add(extractPane);
        workspace = workspacePane;
    }
    
    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new CodeCheckController(app);

        homeButton.setOnAction(e->{
            homePane();
        });       
        previousButton.setOnAction(e->{
            prevPane();
        });     
        nextButton.setOnAction(e->{
            nextPane();
        });
        extractButton.setOnAction(e->{
            controller.handleExtractRequest();
        });
        renameButton.setOnAction(e->{
            controller.handleRenameRequest();
        });
        unzipButton.setOnAction(e->{
            controller.handleUnzipRequest();
        });
        extractCodeButton.setOnAction(e->{
            controller.handleExtractCodeRequest();
        });
        checkButton.setOnAction(e->{
            controller.handleCodeCheckRequest();
        });
        viewResultsButton.setOnAction(e->{
            controller.handleViewResultsRequest();
        });
        removeButton1.setOnAction(e->{
            controller.handleRemoveButton1();
        });
        removeButton2.setOnAction(e->{
            controller.handleRemoveButton2();
        });
        removeButton3.setOnAction(e->{
            controller.handleRemoveButton3();
        });
        removeButton4.setOnAction(e->{
            controller.handleRemoveButton4();
        });
        removeButton5.setOnAction(e->{
            controller.handleRemoveButton5();
        });
        refreshButton1.setOnAction(e->{
            controller.handleRefreshButton1();
        });
        refreshButton2.setOnAction(e->{
            controller.handleRefreshButton2();
        });
        refreshButton3.setOnAction(e->{
            controller.handleRefreshButton3();
        });
        refreshButton4.setOnAction(e->{
            controller.handleRefreshButton4();
        });
        refreshButton5.setOnAction(e->{
            controller.handleRefreshButton5();
        });
        viewButton1.setOnAction(e->{
            controller.handleViewButton1();
        });
        viewButton2.setOnAction(e->{
            controller.handleViewButton2();
        });
        viewButton3.setOnAction(e->{
            controller.handleViewButton3();
        });
        viewButton4.setOnAction(e->{
            controller.handleViewButton4();
        });
        viewButton5.setOnAction(e->{
            controller.handleViewButton5();
        });
    }
    
    
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    
    private void initStyle() {
        editToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        homeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        previousButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        nextButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        extractTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : extractTableView.getColumns())
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        
        
    }
    
    public void homePane() {
        if (currentPaneIndex > 0) {
            workspacePane.getChildren().remove(0);
            workspacePane.getChildren().add(extractPane);
            currentPaneIndex = 0;
            homeButton.setDisable(true);
            previousButton.setDisable(true);
            nextButton.setDisable(false);
        }
    }
    
    public void prevPane() {
        if (currentPaneIndex > 0 && currentPaneIndex <= 4) {
            workspacePane.getChildren().remove(0);
            workspacePane.getChildren().add(panes.get(currentPaneIndex-1));
            currentPaneIndex--;
            nextButton.setDisable(false);
            homeButton.setDisable(false);
            if (currentPaneIndex == 0) {
                previousButton.setDisable(true);
                homeButton.setDisable(true);
            }             
        }           
    }
    
    public void nextPane() {
        if (currentPaneIndex >= 0 && currentPaneIndex < 4) {
            workspacePane.getChildren().remove(0);
            workspacePane.getChildren().add(panes.get(currentPaneIndex+1));
            currentPaneIndex++;
            previousButton.setDisable(false);
            homeButton.setDisable(false);
            if (currentPaneIndex == 4)
                nextButton.setDisable(true);
        }    
    }

    @Override
    public void resetWorkspace() {
        homeButton.setDisable(true);
        previousButton.setDisable(true);
        nextButton.setDisable(false);
        CodeCheckData data = (CodeCheckData)app.getDataComponent();
        data.resetData();
        extractTableView.getItems().clear();
        renameTableView.getItems().clear();
        unzipTableView.getItems().clear();
        extractCodeTableView.getItems().clear();
        checkTableView.getItems().clear();
        successBox1.clear();
        successBox2.clear();
        successBox3.clear();
        successBox4.clear();
        successBox5.clear();
        errorBox1.clear();
        errorBox2.clear();
        errorBox3.clear();
        errorBox4.clear();
        homePane();
    }
    
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {

    }

}
