package cc.workspace;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.image.Image;
import cc.CodeCheckApp;
import cc.data.CodeCheckData;
import cc.data.Slide;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import org.apache.commons.io.FileUtils;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file toolbar.
 *
 * @author James Li
 * @version 1.0
 */
public class CodeCheckController {

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    CodeCheckApp app;
    ReentrantLock progressLock;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public CodeCheckController(CodeCheckApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }

    public void handleExtractRequest() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.successBox1.clear();
        workspace.errorBox1.clear();
        workspace.successBox1.appendText("Successfully extracted files: \n");
        workspace.errorBox1.appendText("Submission Errors: \n");
        ArrayList<String> errorFiles = new ArrayList();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        String destination = "work/" + title + "/submissions/";
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> selectedSlides = workspace.extractTableView.getSelectionModel().getSelectedItems();
        data.getSlides2().clear();
        Service service = new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        double progress = 0;
                        for (Slide s : selectedSlides) {
                            try {
                                ZipFile zipFile = new ZipFile(s.getPath());
                                List fileHeaderList = zipFile.getFileHeaders();
                                for (int i = 0; i < fileHeaderList.size(); i++) {
                                    FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                                    zipFile.extractFile(fileHeader, destination);
                                    if (!fileHeader.getFileName().endsWith(".txt") && !fileHeader.getFileName().equals((".DS_Store"))
                                            && !fileHeader.getFileName().endsWith(".rar")) {
                                        workspace.successBox1.appendText("-" + fileHeader.getFileName() + "\n");
                                        File file = new File(destination + fileHeader.getFileName());
                                        data.addSlide2(fileHeader.getFileName(), file.getPath());
                                    } else if (fileHeader.getFileName().endsWith(".rar")) {
                                        errorFiles.add(fileHeader.getFileName());
                                    }
                                    updateProgress(i, fileHeaderList.size());
                                    progress = i;
                                    // SLEEP EACH FRAME
                                    Thread.sleep(10);
                                }
                                updateProgress(progress, 1);
                                //zipFile.extractAll(destination);
                            } catch (ZipException ex) {

                            }
                        }
                        return null;
                    }
                };
            }
        };
        // THIS GETS THE THREAD ROLLING
        workspace.stepOneProgressBar.progressProperty().bind(service.progressProperty());
        workspace.stepOneProgressIndicator.progressProperty().bind(service.progressProperty());
        service.start();
        workspace.renameTableView.getItems().clear();

        if (!errorFiles.isEmpty()) {
            for (String s : errorFiles) {
                workspace.errorBox1.appendText("-" + s + "\n");
            }
        } else {
            workspace.errorBox1.appendText("-none");
        }
        workspace.extractButton.setDisable(true);
    }

    public void handleRenameRequest() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.successBox2.clear();
        workspace.errorBox2.clear();
        workspace.successBox2.appendText("Successfully renamed submissions: \n");
        workspace.errorBox2.appendText("Rename Errors: \n");
        ArrayList<String> errorFiles = new ArrayList();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        String destination = "work/" + title + "/submissions/";
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> slides = workspace.renameTableView.getItems();
        workspace.unzipTableView.getItems().clear();
        data.getSlides3().clear();
        Service service = new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        double progress = 0;
                        for (Slide s : slides) {
                            progress++;
                            File file = new File(s.getPath());
                            try {
                                String[] split = s.getPath().split("_");
                                String newName = split[1];
                                File newFile = new File(destination + newName + ".zip");
                                file.renameTo(newFile);
                                workspace.successBox2.appendText("-" + s.getFileName() + "\n" + "becomes " + newName + "\n");
                                data.addSlide3(newFile.getName(), newFile.getPath());
                            } catch (ArrayIndexOutOfBoundsException e) {
                                errorFiles.add(s.getFileName());
                            }
                            updateProgress(progress, slides.size());
                            Thread.sleep(10);
                        }
                        return null;
                    }
                };
            }
        };

        workspace.stepTwoProgressBar.progressProperty().bind(service.progressProperty());
        workspace.stepTwoProgressIndicator.progressProperty().bind(service.progressProperty());
        service.start();
        workspace.unzipTableView.getItems().clear();
        //data.getSlides3().clear();

        if (!errorFiles.isEmpty()) {
            for (String s : errorFiles) {
                workspace.errorBox2.appendText("-" + s + "\n");
            }
        } else {
            workspace.errorBox2.appendText("-none");
        }
        workspace.renameButton.setDisable(true);
    }

    public void handleUnzipRequest() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.successBox3.clear();
        workspace.errorBox3.clear();
        workspace.successBox3.appendText("Successfully unzipped files: \n");
        workspace.errorBox3.appendText("Unzip Errors: \n");
        ArrayList<String> errorFiles = new ArrayList<>();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        String destination = "work/" + title + "/projects/";
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> selectedSlides = workspace.unzipTableView.getSelectionModel().getSelectedItems();
        workspace.extractCodeTableView.getItems().clear();
        data.getSlides4().clear();
        Service service = new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        double progress = 0;
                        for (Slide s : selectedSlides) {
                            progress++;
                            try {
                                String id = s.getFileName().substring(0, s.getFileName().indexOf(".zip"));
                                Path path = Paths.get(destination + id);
                                Files.createDirectories(path);
                                ZipFile zipFile = new ZipFile(s.getPath());
                                zipFile.extractAll(destination + id);
                                File file = new File(destination + id);
                                workspace.successBox3.appendText("-" + s.getFileName() + "\n");
                                if (!file.getName().endsWith(".txt") && !file.getName().equals((".DS_Store"))) {
                                    data.addSlide4(file.getName(), file.getPath());
                                }
                            } catch (IOException | ZipException ex) {
                                errorFiles.add(s.getFileName());
                            }
                            updateProgress(progress, selectedSlides.size());
                            Thread.sleep(10);
                        }
                        return null;
                    }
                };
            }
        };

        workspace.stepThreeProgressBar.progressProperty().bind(service.progressProperty());
        workspace.stepThreeProgressIndicator.progressProperty().bind(service.progressProperty());
        service.start();
        //workspace.extractCodeTableView.getItems().clear();
        //data.getSlides4().clear();

//        File[] unzippedFiles = new File("work/" + title + "/projects/").listFiles();
//        data.getSlides4().clear();
//        for (File x : unzippedFiles) {
//            if (!x.getName().endsWith(".txt") && !x.getName().equals((".DS_Store"))) {
//                data.addSlide4(x.getName(), x.getPath());
//            }
//        }
        if (!errorFiles.isEmpty()) {
            for (String s : errorFiles) {
                workspace.errorBox3.appendText("-" + s + "\n");
            }
        } else {
            workspace.errorBox3.appendText("-none");
        }
        workspace.unzipButton.setDisable(true);
    }

    public void handleExtractCodeRequest() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.successBox4.clear();
        workspace.errorBox4.clear();
        workspace.successBox4.appendText("Successfully code extraction: \n");
        workspace.errorBox4.appendText("Code Extraction Errors: \n");
        ArrayList<String> errorFiles = new ArrayList<>();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        String destination = "work/" + title + "/code/";
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> selectedSlides = workspace.extractCodeTableView.getSelectionModel().getSelectedItems();
        workspace.checkTableView.getItems().clear();
        data.getSlides5().clear();
        ArrayList<String> extractTypes = new ArrayList<>();
        Service service = new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        double progress = 0;
                        for (Slide s : selectedSlides) {
                            progress++;
                            File dir = new File(s.getPath());
                            if (workspace.javaCheckBox.isSelected()) {
                                extractTypes.add("java");
                            }
                            if (workspace.jsCheckBox.isSelected()) {
                                extractTypes.add("js");
                            }
                            if (workspace.chcppCheckBox.isSelected()) {
                                extractTypes.add("c");
                                extractTypes.add("h");
                                extractTypes.add("cpp");
                            }
                            if (workspace.csCheckBox.isSelected()) {
                                extractTypes.add("cs");
                            }
                            if (workspace.otherCheckBox.isSelected()) {
                                extractTypes.add(workspace.otherLabel.getText());
                            }
                            Path path = Paths.get(destination + s.getFileName());
                            workspace.successBox4.appendText("-" + s.getFileName() + "\n");              
                            try {
                                Files.createDirectories(path);
                                File newDir = new File(destination + s.getFileName());
                                String[] extractArray = new String[extractTypes.size()];
                                extractArray = extractTypes.toArray(extractArray);
                                boolean recursive = true;
                                Collection files = FileUtils.listFiles(dir, extractArray, recursive);
                                double it = 0;
                                for (Iterator iterator = files.iterator(); iterator.hasNext();) {
                                    it++;
                                    File file = (File) iterator.next();
                                    File source = new File(file.getAbsolutePath());
                                    File dest = new File(destination + s.getFileName());
                                    try {
                                        FileUtils.copyFileToDirectory(source, dest);
                                        workspace.successBox4.appendText("---" + file.getName() + "\n");                                  
                                    } catch (IOException e) {
                                        errorFiles.add(s.getFileName());
                                    }
                                    updateProgress(it, files.size());
                                    Thread.sleep(10);
                                    progress = it;
                                }
                                if (!newDir.getName().endsWith(".txt") && !newDir.getName().equals((".DS_Store"))) {
                                            data.addSlide5(newDir.getName(), newDir.getPath());
                                }
                                updateProgress(progress, 1);
                            } catch (IOException ex) {

                            }

                        }
                        return null;
                    }
                };
            }
        };
        workspace.stepFourProgressBar.progressProperty().bind(service.progressProperty());
        workspace.stepFourProgressIndicator.progressProperty().bind(service.progressProperty());
        service.start();
        workspace.checkTableView.getItems().clear();

//        File[] extractedFiles = new File("work/" + title + "/code/").listFiles();
//        data.getSlides5().clear();
//        for (File x : extractedFiles) {
//            if (!x.getName().endsWith(".txt") && !x.getName().equals((".DS_Store"))) {
//                data.addSlide5(x.getName(), x.getPath());
//            }
//        }
        if (!errorFiles.isEmpty()) {
            for (String s : errorFiles) {
                workspace.errorBox4.appendText("-" + s + "\n");
            }
        } else {
            workspace.errorBox4.appendText("-none");
        }
        workspace.extractCodeButton.setDisable(true);
    }

    public void handleCodeCheckRequest() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.successBox5.clear();
        Service service = new Service() {
            @Override
            protected Task createTask() {
                return new Task() {
                    @Override
                    protected Object call() throws Exception {
                        for (int i = 0; i < 50; i++) {
                            updateProgress(i, 49);
                            try {
                                Thread.sleep(10);

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        workspace.successBox5.appendText("Student Plagiarism Check results can be found at \n");
                        workspace.successBox5.appendText("https://www.cs.stonybrook.edu/");
                        return null;
                    }
                };
            }
        };
        workspace.stepFiveProgressBar.progressProperty().bind(service.progressProperty());
        workspace.stepFiveProgressIndicator.progressProperty().bind(service.progressProperty());
        service.start();

        workspace.checkButton.setDisable(true);
    }

    public void handleViewResultsRequest() {
        WebView browser = new WebView();
        WebEngine engine = browser.getEngine();
        engine.load("https://www.binghamton.edu/");
        Stage stage = new Stage();
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());
        VBox pane = new VBox();
        pane.getChildren().add(browser);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.setTitle("Code Check Results");
        stage.show();
    }

    public void handleRemoveButton1() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> selectedSlides = workspace.extractTableView.getSelectionModel().getSelectedItems();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Remove");
        alert.setContentText("Do you want to remove this item?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            for (Slide s : selectedSlides) {
                workspace.extractTableView.getItems().remove(s);
                try {
                    Files.deleteIfExists(Paths.get(s.getPath()));
                } catch (IOException ex) {

                }
            }
        } else {

        }
    }

    public void handleRemoveButton2() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> selectedSlides = workspace.renameTableView.getSelectionModel().getSelectedItems();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Remove");
        alert.setContentText("Do you want to remove this item?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            for (Slide s : selectedSlides) {
                workspace.renameTableView.getItems().remove(s);
                try {
                    Files.deleteIfExists(Paths.get(s.getPath()));
                } catch (IOException ex) {

                }
            }
        } else {

        }
    }

    public void handleRemoveButton3() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> selectedSlides = workspace.unzipTableView.getSelectionModel().getSelectedItems();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Remove");
        alert.setContentText("Do you want to remove this item?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            for (Slide s : selectedSlides) {
                workspace.unzipTableView.getItems().remove(s);
                try {
                    Files.deleteIfExists(Paths.get(s.getPath()));
                } catch (IOException ex) {

                }
            }
        } else {

        }
    }

    public void handleRemoveButton4() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> selectedSlides = workspace.extractCodeTableView.getSelectionModel().getSelectedItems();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Remove");
        alert.setContentText("Do you want to remove this item?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            for (Slide s : selectedSlides) {
                workspace.extractCodeTableView.getItems().remove(s);
                try {
                    FileUtils.deleteDirectory(new File(s.getPath()));
                } catch (IOException ex) {

                }
            }
        } else {

        }
    }

    public void handleRemoveButton5() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        ObservableList<Slide> selectedSlides = workspace.checkTableView.getSelectionModel().getSelectedItems();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Remove");
        alert.setContentText("Do you want to remove this item?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            for (Slide s : selectedSlides) {
                workspace.checkTableView.getItems().remove(s);
                try {
                    FileUtils.deleteDirectory(new File(s.getPath()));
                } catch (IOException ex) {

                }
            }
        } else {

        }
    }

    public void handleRefreshButton1() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.extractTableView.getItems().clear();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        File[] files = new File("work/" + title + "/" + "blackboard").listFiles();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        for (File f : files) {
            if (!f.getName().equals(".DS_Store")) {
                data.addSlide1(f.getName(), f.getPath());
            }
        }
        workspace.extractTableView.setItems(data.getSlides1());
    }

    public void handleRefreshButton2() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.renameTableView.getItems().clear();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        File[] files = new File("work/" + title + "/" + "submissions").listFiles();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        for (File f : files) {
            if (!f.getName().equals(".DS_Store") && !f.getName().endsWith(".txt")) {
                data.addSlide2(f.getName(), f.getPath());
            }
        }
        workspace.renameTableView.setItems(data.getSlides2());
    }

    public void handleRefreshButton3() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.unzipTableView.getItems().clear();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        File[] files = new File("work/" + title + "/" + "submissions").listFiles();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        for (File f : files) {
            if (!f.getName().equals(".DS_Store") && !f.getName().endsWith(".txt")) {
                data.addSlide3(f.getName(), f.getPath());
            }
        }
        workspace.unzipTableView.setItems(data.getSlides3());
    }

    public void handleRefreshButton4() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.extractCodeTableView.getItems().clear();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        File[] files = new File("work/" + title + "/" + "projects").listFiles();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        for (File f : files) {
            if (!f.getName().equals(".DS_Store")) {
                data.addSlide4(f.getName(), f.getPath());
            }
        }
        workspace.extractCodeTableView.setItems(data.getSlides4());
    }

    public void handleRefreshButton5() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        workspace.checkTableView.getItems().clear();
        String title = app.getGUI().getWindow().getTitle().substring(13);
        File[] files = new File("work/" + title + "/" + "code").listFiles();
        CodeCheckData data = (CodeCheckData) app.getDataComponent();
        for (File f : files) {
            if (!f.getName().equals(".DS_Store")) {
                data.addSlide5(f.getName(), f.getPath());
            }
        }
        workspace.checkTableView.setItems(data.getSlides5());
    }

    public void handleViewButton1() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        Slide s = workspace.extractTableView.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Contents of " + s.getFileName());
        StringWriter writer = new StringWriter();
        try {
            ZipFile zipFile = new ZipFile(s.getPath());
            List fileHeaderList = zipFile.getFileHeaders();
            for (int i = 0; i < fileHeaderList.size(); i++) {
                FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                writer.write(fileHeader.getFileName() + "\n");
            }
        } catch (ZipException ex) {

        }
        TextArea textArea = new TextArea(writer.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();
    }

    public void handleViewButton2() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        Slide s = workspace.renameTableView.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Contents of " + s.getFileName());
        StringWriter writer = new StringWriter();
        try {
            ZipFile zipFile = new ZipFile(s.getPath());
            List fileHeaderList = zipFile.getFileHeaders();
            for (int i = 0; i < fileHeaderList.size(); i++) {
                FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                writer.write(fileHeader.getFileName() + "\n");
            }

        } catch (ZipException ex) {

        }
        TextArea textArea = new TextArea(writer.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();

    }

    public void handleViewButton3() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        Slide s = workspace.unzipTableView.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Contents of " + s.getFileName());
        StringWriter writer = new StringWriter();
        try {
            ZipFile zipFile = new ZipFile(s.getPath());
            List fileHeaderList = zipFile.getFileHeaders();
            for (int i = 0; i < fileHeaderList.size(); i++) {
                FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                writer.write(fileHeader.getFileName() + "\n");
            }

        } catch (ZipException ex) {

        }
        TextArea textArea = new TextArea(writer.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();

    }

    public void handleViewButton4() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        Slide s = workspace.extractCodeTableView.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Contents of " + s.getFileName());
        StringWriter writer = new StringWriter();

        File f = new File(s.getPath());
        ArrayList<File> files = new ArrayList<File>(Arrays.asList(f.listFiles()));
        for (File file : files) {
            writer.write(file.getName() + "\n");
        }

        TextArea textArea = new TextArea(writer.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();

    }

    public void handleViewButton5() {
        CodeCheckWorkspace workspace = (CodeCheckWorkspace) app.getWorkspaceComponent();
        Slide s = workspace.checkTableView.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Code Check");
        alert.setHeaderText("Contents of " + s.getFileName());
        StringWriter writer = new StringWriter();

        File f = new File(s.getPath());
        ArrayList<File> files = new ArrayList<File>(Arrays.asList(f.listFiles()));
        for (File file : files) {
            writer.write(file.getName() + "\n");
        }

        TextArea textArea = new TextArea(writer.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        alert.showAndWait();

    }

    // THIS HELPER METHOD LOADS AN IMAGE SO WE CAN SEE IT'S SIZE
    private Image loadImage(String imagePath) throws MalformedURLException {
        File file = new File(imagePath);
        URL fileURL = file.toURI().toURL();
        Image image = new Image(fileURL.toExternalForm());
        return image;
    }
}
